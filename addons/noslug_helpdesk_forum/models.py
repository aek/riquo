import odoo
from odoo.addons.noslug.models import slug

if odoo.addons.website_helpdesk_forum:
    odoo.addons.website_helpdesk_forum.models.helpdesk.slug = slug
