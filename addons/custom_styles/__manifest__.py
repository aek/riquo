# -*- coding: utf-8 -*-
{
    'name': "Custom Styles",
    'summary': """Add custom styles for website""",
    'description': "",
    'category': 'Website',
    'version': '1.0',
    'depends': ['website'],
    'data': [
        'views.xml'
    ],
}
