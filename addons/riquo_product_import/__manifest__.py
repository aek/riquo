# -*- coding: utf-8 -*-
{
    'name': "Riquo Custom Product Import",
    'summary': """Riquo Custom Product Import""",
    'description': "",
    'category': 'tools',
    'version': '1.0',
    'depends': ['base', 'product', 'sales_team'],
    'data': [
        'views.xml'
    ],
}
