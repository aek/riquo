import base64
from StringIO import StringIO
from xml.sax import handler, parse

from odoo import api, models, fields, _

xml2odoo = {
    'UnitID': 'name', #'xml_unitid',
    'LotID': 'xml_lotid',
    'Asset Tag': 'xml_asset_tag',
    'Receiving Date': 'xml_receiving_date',
    'Category': 'xml_category',
    'Manufacturer': 'xml_manufacturer',
    'Model': 'xml_model',
    'Part Number': 'xml_part_number',
    'Serial Number': 'xml_serial_number',
    'Display Size': 'xml_display_size',
    'Resolution': 'xml_resolution',
    'Processor': 'xml_processor',
    'Speed': 'xml_speed',
    'Procs': 'xml_procs',
    'HDD1 Size': 'xml_hdd1_size',
    'HDD1 Model': 'xml_hdd1_model',
    'HDD1 Serial': 'xml_hdd1_serial',
    'HDD2 Size': 'xml_hdd2_size',
    'HDD2 Model': 'xml_hdd2_model',
    'HDD2 Serial': 'xml_hdd2_serial',
    'HDD3 Size': 'xml_hdd3_size',
    'HDD3 Model': 'xml_hdd3_model',
    'HDD3 Serial': 'xml_hdd3_serial',
    'HDD4 Size': 'xml_hdd4_size',
    'HDD4 Model': 'xml_hdd4_model',
    'HDD4 Serial': 'xml_hdd4_serial',
    'RAM': 'xml_ram',
    'Optical': 'xml_optical',
    'Chassis': 'xml_chassis',
    'Keyboard': 'xml_keyboard',
    'Webcam': 'xml_webcam',
    'Battery Model': 'xml_battery_model',
    'VideoCard': 'xml_videocard',
    'COA': 'xml_coa',
    'OS Installed': 'xml_os_installed',
    'COA Number': 'xml_coa_number',
    'New COA Number': 'xml_new_coa_number',
    'Defect Codes': 'xml_defect_codes',
    'Expanded Codes': 'xml_expanded_codes',
    'Comments': 'xml_comments',
    'Grade': 'xml_grade',
    'Test End': 'xml_test_end',
    'Cost': 'standard_price', #'xml_cost',
    'Price': 'list_price', #'xml_price',
    'Exported': 'xml_exported',
    'PictureID': 'xml_pictureid',
    'User': 'xml_user',
    'MB Manuf': 'xml_mb_manuf',
    'MB Model': 'xml_mb_model',
    'MB Serial': 'xml_mb_serial',
    'MB Version': 'xml_mb_version',
    'CPU Socket': 'xml_cpu_socket',
    'RAM1 Size': 'xml_ram1_size',
    'RAM1 Model': 'xml_ram1_model',
    'RAM1 Serial': 'xml_ram1_serial',
    'RAM1 Type': 'xml_ram1_type',
    'RAM2 Size': 'xml_ram2_size',
    'RAM2 Model': 'xml_ram2_model',
    'RAM2 Serial': 'xml_ram2_serial',
    'RAM2 Type': 'xml_ram2_type',
    'RAM3 Size': 'xml_ram3_size',
    'RAM3 Model': 'xml_ram3_model',
    'RAM3 Serial': 'xml_ram3_serial',
    'RAM3 Type': 'xml_ram3_type',
    'RAM4 Size': 'xml_ram4_size',
    'RAM4 Model': 'xml_ram4_model',
    'RAM4 Serial': 'xml_ram4_serial',
    'RAM4 Type': 'xml_ram4_type',
    'Optical Serial': 'xml_optical_serial',
    'Battery Duration': 'xml_battery_duration',
}


class ExcelHandler(handler.ContentHandler):
    def __init__(self):
        self.chars = []
        self.cells = []
        self.rows = []
        self.tables = []

    def characters(self, content):
        self.chars.append(content)

    def startElement(self, name, atts):
        if name == "Cell":
            self.chars = []
        elif name == "Row":
            self.cells = []
        elif name == "Table":
            self.rows = []

    def endElement(self, name):
        if name == "Cell":
            self.cells.append(''.join(self.chars))
        elif name == "Row":
            self.rows.append(self.cells)
        elif name == "Table":
            self.tables.append(self.rows)


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    # xml_unitid = fields.Char('UnitID')
    xml_lotid = fields.Char('LotID')
    xml_asset_tag = fields.Char('Asset Tag')
    xml_receiving_date = fields.Char('Receiving Date')
    xml_category = fields.Char('Category')
    xml_manufacturer = fields.Char('Manufacturer')
    xml_model = fields.Char('Model')
    xml_part_number = fields.Char('Part Number')
    xml_serial_number = fields.Char('Serial Number')
    xml_display_size = fields.Char('Display Size')
    xml_resolution = fields.Char('Resolution')
    xml_processor = fields.Char('Processor')
    xml_speed = fields.Char('Speed')
    xml_procs = fields.Char('Procs')
    xml_hdd1_size = fields.Char('HDD1 Size')
    xml_hdd1_model = fields.Char('HDD1 Model')
    xml_hdd1_serial = fields.Char('HDD1 Serial')
    xml_hdd2_size = fields.Char('HDD2 Size')
    xml_hdd2_model = fields.Char('HDD2 Model')
    xml_hdd2_serial = fields.Char('HDD2 Serial')
    xml_hdd3_size = fields.Char('HDD3 Size')
    xml_hdd3_model = fields.Char('HDD3 Model')
    xml_hdd3_serial = fields.Char('HDD3 Serial')
    xml_hdd4_size = fields.Char('HDD4 Size')
    xml_hdd4_model = fields.Char('HDD4 Model')
    xml_hdd4_serial = fields.Char('HDD4 Serial')
    xml_ram = fields.Char('RAM')
    xml_optical = fields.Char('Optical')
    xml_chassis = fields.Char('Chassis')
    xml_keyboard = fields.Char('Keyboard')
    xml_webcam = fields.Char('Webcam')
    xml_battery_model = fields.Char('Battery Model')
    xml_videocard = fields.Char('VideoCard')
    xml_coa = fields.Char('COA')
    xml_os_installed = fields.Char('OS Installed')
    xml_coa_number = fields.Char('COA Number')
    xml_new_coa_number = fields.Char('New COA Number')
    xml_defect_codes = fields.Char('Defect Codes')
    xml_expanded_codes = fields.Char('Expanded Codes')
    xml_comments = fields.Char('Comments')
    xml_grade = fields.Char('Grade')
    xml_test_end = fields.Char('Test End')
    # xml_cost = fields.Char('Cost')
    # xml_price = fields.Char('Price')
    xml_exported = fields.Char('Exported')
    xml_pictureid = fields.Char('PictureID')
    xml_user = fields.Char('User')
    xml_mb_manuf = fields.Char('MB Manuf')
    xml_mb_model = fields.Char('MB Model')
    xml_mb_serial = fields.Char('MB Serial')
    xml_mb_version = fields.Char('MB Version')
    xml_cpu_socket = fields.Char('CPU Socket')
    xml_ram1_size = fields.Char('RAM1 Size')
    xml_ram1_model = fields.Char('RAM1 Model')
    xml_ram1_serial = fields.Char('RAM1 Serial')
    xml_ram1_type = fields.Char('RAM1 Type')
    xml_ram2_size = fields.Char('RAM2 Size')
    xml_ram2_model = fields.Char('RAM2 Model')
    xml_ram2_serial = fields.Char('RAM2 Serial')
    xml_ram2_type = fields.Char('RAM2 Type')
    xml_ram3_size = fields.Char('RAM3 Size')
    xml_ram3_model = fields.Char('RAM3 Model')
    xml_ram3_serial = fields.Char('RAM3 Serial')
    xml_ram3_type = fields.Char('RAM3 Type')
    xml_ram4_size = fields.Char('RAM4 Size')
    xml_ram4_model = fields.Char('RAM4 Model')
    xml_ram4_serial = fields.Char('RAM4 Serial')
    xml_ram4_type = fields.Char('RAM4 Type')
    xml_optical_serial = fields.Char('Optical Serial')
    xml_battery_duration = fields.Char('Battery Duration')


class RiquoProductImport(models.TransientModel):
    _name = 'riquo.product.import'

    file = fields.Binary('File to Import', required=True)

    @api.multi
    def riquo_import(self):
        self.ensure_one()
        prod_ids = []
        excel_handler = ExcelHandler()
        raw = base64.decodestring(self.file)

        parse(StringIO(raw), excel_handler)
        first_row = False
        for row in excel_handler.rows:
            if first_row is False:
                first_row = []
                for index in range(len(row)):
                    if xml2odoo.get(row[index], False):
                        first_row.append(xml2odoo[row[index]])
                    else:
                        first_row.append(row[index])
                continue
            data = {}
            for index in range(len(row)):
                if first_row[index] and row[index]:
                    data[first_row[index]] = row[index]

            prod_id = self.env['product.template'].create(data)
            prod_ids.append(prod_id.id)

        context = dict(self._context or {})
        tree_id = self.env.ref('riquo_product_import.riquo_custom_product_import_tree').id

        return {
            'name': _('Imported Products'),
            'view_type': 'form',
            'view_mode': 'tree, form',
            'res_model': 'product.template',
            'view_id': tree_id,
            'views': [(tree_id, 'tree'), (False, 'form')],
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', prod_ids)],
            'context': context,
        }
