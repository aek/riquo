import odoo
from odoo import models
from odoo.addons.website.models.ir_http import ModelConverter, PageConverter
from odoo.osv.orm import browse_record

import werkzeug

def slug(value):
    if isinstance(value, models.BaseModel):
        identifier = value.id
    else:
        # assume name_search result tuple
        identifier, name = value
    return str(identifier)

class ModelConverterSlug(ModelConverter):

    def to_url(self, value):
        return slug(value)

class Http(models.AbstractModel):
    _inherit = 'ir.http'

    @classmethod
    def _get_converters(cls):
        """ Get the converters list for custom url pattern werkzeug need to
            match Rule. This override adds the website ones.
        """
        return dict(
            super(Http, cls)._get_converters(),
            model=ModelConverterSlug,
            page=PageConverter,
        )

class QueryURL(object):
    def __init__(self, path='', path_args=None, **args):
        self.path = path
        self.args = args
        self.path_args = set(path_args or [])

    def __call__(self, path=None, path_args=None, **kw):
        path = path or self.path
        for key, value in self.args.items():
            kw.setdefault(key, value)
        path_args = set(path_args or []).union(self.path_args)
        paths, fragments = [], []
        for key, value in kw.items():
            if value and key in path_args:
                if isinstance(value, browse_record):
                    paths.append((key, slug(value)))
                else:
                    paths.append((key, value))
            elif value:
                if isinstance(value, list) or isinstance(value, set):
                    fragments.append(werkzeug.url_encode([(key, item) for item in value]))
                else:
                    fragments.append(werkzeug.url_encode([(key, value)]))
        for key, value in paths:
            path += '/' + key + '/%s' % value
        if fragments:
            path += '?' + '&'.join(fragments)
        return path

odoo.addons.website.models.website.slug = slug

if odoo.addons.website_sale:
    odoo.addons.website_sale.controllers.main.slug = slug
    odoo.addons.website_sale.controllers.main.QueryURL = QueryURL

if odoo.addons.website_forum:
    odoo.addons.website_forum.controllers.main.slug = slug
if odoo.addons.website_blog:
    odoo.addons.website_blog.controllers.main.slug = slug
    odoo.addons.website_blog.controllers.main.QueryURL = QueryURL

if odoo.addons.survey:
    odoo.addons.survey.models.survey.slug = slug

if odoo.addons.website_slides:
    odoo.addons.website_slides.models.slides.slug = slug

if odoo.addons.website_event_track:
    odoo.addons.website_event_track.models.event.slug = slug

if odoo.addons.website_livechat:
    odoo.addons.website_livechat.models.im_livechat.slug = slug

if odoo.addons.website_partner:
    odoo.addons.website_partner.models.res_partner.slug = slug

if odoo.addons.website_hr_recruitment:
    odoo.addons.website_hr_recruitment.controllers.main.slug = slug