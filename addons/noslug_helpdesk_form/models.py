import odoo
from odoo.addons.noslug.models import slug

if odoo.addons.website_helpdesk_form:
    odoo.addons.website_helpdesk_form.models.helpdesk.slug = slug
