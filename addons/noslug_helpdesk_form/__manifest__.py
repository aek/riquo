# -*- coding: utf-8 -*-
{
    'name': "No Slug for website_helpdesk_forum",
    'summary': """No Slug for website_helpdesk_forum""",
    'description': "",
    'category': 'Website',
    'version': '1.0',
    'depends': ['website', 'website_helpdesk_form', 'noslug'],
    'auto_install': True,
}
